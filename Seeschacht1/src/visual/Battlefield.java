package visual;

import java.awt.Container;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JOptionPane;

import controller.Observer;
import controller.Shootable;
import controller.Subject;
import seeschlacht.CellState;
import seeschlacht.Deck;
import seeschlacht.Setup;
import seeschlacht.Ship;

public class Battlefield extends JFrame implements Observer {
	
	
	private Container checkboard;
	private JButton [][] cells = new JButton [Setup.getGridDimensions()][Setup.getGridDimensions()];
	private ImageIcon shot = new ImageIcon("Pricel.png");
	private ImageIcon victory = new ImageIcon("victory.jpg");
	private ImageIcon fire = new ImageIcon("fire.jpg");
	private ImageIcon debris = new ImageIcon("debris.jpg");
	private Subject model;
	private Shootable game;
	
	//ButtonHandler controller = new ButtonHandler();
	
	public void setGame(Shootable game) {
		this.game = game;
	}

	public Battlefield(){
		super("Waffen schussbereit!");
		initBattleField();
	}
	
	public Battlefield(Shootable game, Subject subj){
		super("Waffen schussbereit!");
		setSubject(subj);
		this.game=game;
		initBattleField();
		
	}

	
	private void initBattleField() {
		checkboard = getContentPane();
		checkboard.setLayout(new GridLayout(Setup.getGridDimensions(), Setup.getGridDimensions()));
		
		
		for ( int column = 0; column < cells.length; column++) {
			for ( int row = 0; row < cells.length; row++) {
				cells[column][row]= new JButton(); //column+""+row - Numerierung der Kn�pfe
				checkboard.add(cells[column][row]);
				final int y=column; // Koordinaten verdreht wegen Buttons Layout
				final int x=row;
				cells[column][row].addActionListener((ActionEvent event) -> {
					game.getShot(x,y);
					
				});

			}
		}
		
		
		setSize(600,600);
		setLocationRelativeTo(null);
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		setVisible(true);
		
	}
	
	

	private void displayGameOver(){
		Object[] options = {"Nochmal", "Genug gespielt"};
		int choice = JOptionPane.showOptionDialog(this,  null, "Du hast gewonnen!", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE, victory, options, options[0]);
		if(choice == 0) {this.dispose(); }
		else System.exit(0);
		
	}

	@Override
	public void setSubject(Subject subj) {
		model=subj;
		
	}

	@Override
	public  void update(int x, int y, CellState state) {// Koordinaten verdreht wegen Buttons Layout
		
		switch (state) {
		default:
			cells[y][x].setIcon(shot);
			break;
		case SHIP_HIT:
			cells[y][x].setIcon(fire);
			break;
		case SHIP_SUNK:
			cells[y][x].setIcon(debris);
			break;
		case GAME_OVER:
			displayGameOver();
		}
		
		
		
	}
		

	
	
}
