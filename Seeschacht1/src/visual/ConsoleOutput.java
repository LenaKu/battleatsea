package visual;

import controller.Observer;
import controller.Shootable;
import controller.Subject;
import seeschlacht.CellState;
import seeschlacht.Setup;

public class ConsoleOutput implements Observer {
	Subject model;
	StringBuffer [] output;
	
	
	public ConsoleOutput(){
		output=new StringBuffer[Setup.getGridDimensions()];
		for (int i = 0; i < output.length; i++) {
			output[i]= new StringBuffer (baseLine(Setup.getGridDimensions()));
			
		}
	}

	@Override
	public void setSubject(Subject subj) {
		model=subj;
		
	}

	@Override
	public void update(int x, int y, CellState state) {
		
		System.out.println("ConsoleOutput.update() start");
		switch(state){
		case EMPTY:
			setCell(x,y,"*"); break;
		case SHIP_HIT: setCell(x,y,"�"); break;
		case SHIP_SUNK: setCell(x,y,"X"); break;
		case GAME_OVER: System.out.println("Game over!");
			
		}
		if(state !=CellState.GAME_OVER) showBattle();
		System.out.println("ConsoleOutput.update() end");	
		
		
		
	}
	public String baseLine(int gridWidth){
		String line="";
		for (int i = 0; i < gridWidth; i++) {
			line+="- ";
		
		}
		return line;
	}
	
	public void showBattle(){
		System.out.println("Current state:");
		for (int i = 0; i < output.length; i++) {
			System.out.println(output[i]);
		}
		System.out.println("Message end");
	}
	
	private void setCell(int x, int y, String change){
		output[y].replace(x*2, x*2+1, change);
		
	}
	
	
}
