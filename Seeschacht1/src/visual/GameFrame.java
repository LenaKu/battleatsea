package visual;

import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.event.ActionEvent;

import javax.swing.GroupLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JFrame;

import seeschlacht.Seabattle;

public class GameFrame extends JFrame{
	Seabattle game;
	PseudoConsole cons;
	Battlefield field;
	
	
	public void setGame(Seabattle game) {
		this.game = game;
	}
	public GameFrame(){
		initGF();
	}
	public GameFrame(Seabattle game){
		setGame(game);
		initGF(  );
	}

	private void initGF() {
		setTitle("Seeschlacht");
		setSize(300,200);
		setLocationRelativeTo(null);
		setDefaultCloseOperation(DISPOSE_ON_CLOSE);
		
		ImageIcon ship = new ImageIcon("battleShip.jpg");
		setIconImage(ship.getImage());
		
		setLayout(new BorderLayout());
		JButton startButton = new JButton("Start");
		JButton exitButton = new JButton("Exit");
		add(exitButton, BorderLayout.SOUTH);
		add(startButton, BorderLayout.NORTH);
		
		startButton.addActionListener((ActionEvent envent)-> {
			startNewGame();
			
			
		});
		exitButton.addActionListener((ActionEvent envent)-> {
		System.exit(0);
		});
	}

	
//	private void createLayout1(JComponent...args) {
//		
//		Container pane=getContentPane();
//		GroupLayout gl= new GroupLayout(pane);
//		pane.setLayout(gl);
//		
//		gl.setAutoCreateContainerGaps(true);
//		gl.setHorizontalGroup(gl.createSequentialGroup().addComponent(args[0]));
//		gl.setVerticalGroup(gl.createSequentialGroup().addComponent(args[0]));
		
		
		
//	}
	private void startNewGame(){
		if(game==null){setGame(new Seabattle());}
		if(cons!=null){cons.dispose(); }
		if(field!=null){field.dispose();}
		game.startSeabattle();
		 
		cons= new PseudoConsole(game);
		 field=new Battlefield(game,game);
		game.registerObserver(field);
	}
	
	
}
