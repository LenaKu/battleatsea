package visual;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;

import controller.Observer;
import controller.Subject;
import seeschlacht.CellState;

public class FileOutput implements Observer {
	Subject subj;
	Date date; //to be continued
	File log=new File("Seabattle-log.txt");
	PrintWriter pw;
	
	public FileOutput() {
		
	}
	
	public FileOutput(Subject subj) {
		this.subj=subj;
	}
	
	public void prepareLog(){
		if (!log.exists()) {
			try {
				log.createNewFile();
//				System.out.println("File created");
				
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		FileWriter fw;
		try {
			fw = new FileWriter(log);
			pw=new PrintWriter(fw);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
//		System.out.println("file exists");
//		System.out.println((pw==null));
		
	}

	@Override
	public void setSubject(Subject subj) {
		this.subj=subj;
		
	}

	@Override
	public void update(int x, int y, CellState state) {
		pw.println(" "+x+"-"+y+"-->"+state.toString() );
		closeFileOutput();
		
	}
	
	public void closeFileOutput(){
		if(subj.gameIsOver()){
			pw.println("Game overr!");
			pw.flush();
			pw.close();
		}
	}

}
