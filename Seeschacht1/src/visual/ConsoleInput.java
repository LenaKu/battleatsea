package visual;
import java.util.Scanner;

import controller.Shootable;
import seeschlacht.Deck;

public class ConsoleInput {
	Shootable game;
	Scanner sc= new Scanner(System.in);
	private Deck newShot = new Deck();

	public ConsoleInput(Shootable game){
		this.game=game;
		playGame();
	}
	
	
	public Deck prepareShot(){
		System.out.println("Reihe eingeben:");
		try {
			newShot.y=Integer.parseInt(sc.nextLine())-1;
		} catch (Exception e) {
			System.out.println("Versuche noch ein Mal:");
			return null;
		}
		System.out.println("Spalte eingeben:");
		try {
			newShot.x=Integer.parseInt(sc.nextLine())-1;
		} catch (Exception e) {
			System.out.println("Versuche noch ein Mal:");
			return null;
		}
		return newShot;
		
	}
	public void shoot(Deck shot){
		if(shot!=null)
		game.getShot(shot.x, shot.y);
	}
	
	private void playGame(){
		while(!game.gameIsOver()){
			shoot(prepareShot());
		}
		sc.close();
		
	}
	
	
	
	
	
}
