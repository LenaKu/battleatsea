package visual;

import java.awt.BorderLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextArea;
import javax.swing.JTextField;

import controller.Observer;
import seeschlacht.Seabattle;

public class PseudoConsole extends JFrame {
	

	
	private TextPanel outputPanel;
	private InputPanel inputPanel;
	
	public PseudoConsole(){
		setVisible(true);
		setLayout(new BorderLayout());
		setSize(300, 500);
		outputPanel= new TextPanel();
		inputPanel= new InputPanel();
		add(outputPanel, BorderLayout.NORTH);
		add(inputPanel, BorderLayout.CENTER);
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		//System.out.println(dim);
		
		
		
	}
	public PseudoConsole(Seabattle game){
		setVisible(true);
		setLayout(new BorderLayout());
		setSize(300, 500);
		outputPanel= new TextPanel();
		game.registerObserver(outputPanel);
		inputPanel= new InputPanel(game);
		add(outputPanel, BorderLayout.SOUTH);
		add(inputPanel, BorderLayout.CENTER);
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		//System.out.println(dim);
		
		
		
	}
	
	
				
		
	
	
	
}
