package visual;

import java.awt.BorderLayout;

import javax.swing.JPanel;
import javax.swing.JTextArea;

import controller.Observer;
import controller.Subject;
import seeschlacht.CellState;
import seeschlacht.Setup;

public class TextPanel extends JPanel implements Observer {
	private JTextArea textArea;
	private Subject game;
	private StringBuffer [] output;
	
	public TextPanel(){
		textArea= new JTextArea();
		add(textArea, new BorderLayout());
		output=new StringBuffer[Setup.getGridDimensions()];
		for (int i = 0; i < output.length; i++) {
			output[i]= new StringBuffer (baseLine(Setup.getGridDimensions()));
			textArea.append(output[i].toString()+"\n");
		}
	}

	

	@Override
	public void setSubject(Subject subj) {
		this.game=subj;
		
	}

	@Override
	public void update(int x, int y, CellState state) {
		
		switch(state){
		case EMPTY:
			setCell(x,y,"*"); break;
		case SHIP_HIT: setCell(x,y,"�"); break;
		case SHIP_SUNK: setCell(x,y,"X"); break;
		default:;
		
			
		}
		if(state !=CellState.GAME_OVER) showBattle();
	
		
	}
	public String baseLine(int gridWidth){
		String line="";
		for (int i = 0; i < gridWidth; i++) {
			line+="- ";
		
		}
		return line;
	}
	private void setCell(int x, int y, String change){
		output[y].replace(x*2, x*2+1, change);
		
	}
	public void showBattle(){
		textArea.setText("");
		for (int i = 0; i < output.length; i++) {
			textArea.append(output[i]+"\n");
		}
		
	}
}
