package visual;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.border.Border;

import controller.Shootable;
import seeschlacht.Setup;

public class InputPanel extends javax.swing.JPanel {
	private JTextField rowField;
	private JTextField columnField;
	private JLabel rowLabel;
	private JLabel columnLabel;
	private JButton fireBtn;
	private Shootable game;
	
	public InputPanel(){
		rowLabel=new JLabel("Reihe:");
		columnLabel=new JLabel("Spalte:");
		rowField=new JTextField(3);
		columnField= new JTextField(3);
		fireBtn= new JButton("Fire");
		setThisLayout();
		fireBtn.addActionListener((ActionEvent event) -> {
			try{
				int y=Integer.parseInt(rowField.getText());
				int x =Integer.parseInt(columnField.getText());
				game.getShot(x,y);
			}
			catch(NumberFormatException nfe){}
			});
	}
	public InputPanel(Shootable game){
		this.game=game;
		rowLabel=new JLabel("Reihe:");
		columnLabel=new JLabel("Spalte:");
		rowField=new JTextField(3);
		columnField= new JTextField(3);
		fireBtn= new JButton("Fire");
		
		fireBtn.addActionListener((ActionEvent event) -> {
				try{
					int y=Integer.parseInt(rowField.getText());
					int x =Integer.parseInt(columnField.getText());
					if (x>0&&x<=Setup.getGridDimensions()&&y>0&&y<=Setup.getGridDimensions()){
						game.getShot(x-1,y-1);
						}
					else {
						JOptionPane.showMessageDialog(this,
							    "Es gibt nur " + Setup.getGridDimensions()+" Reihen und Spalten");
					}
				}
				catch(NumberFormatException nfe){}
				});
		setThisLayout();
	}
	
	public void setThisLayout(){
		setLayout(new GridBagLayout());
		Border innerBorder = BorderFactory.createTitledBorder("Waffen bereit!");
		setBorder(innerBorder);
		GridBagConstraints gc = new GridBagConstraints();
		
		gc.insets= new Insets(0,5,0,5);
		gc.weightx = 1;
		gc.weighty = 1;
		////////////////////////////// SET ROW-LABEL /////////////////////////////////
		gc.gridx = 0;
		gc.gridy = 1;
		gc.anchor = GridBagConstraints.LINE_END;
		this.add(rowLabel, gc);
		/////////////////////////////////// SET ROWFIELD /////////////////////////////
		gc.gridx=1;
		gc.gridy=1;
		gc.anchor = GridBagConstraints.LINE_START;
		add(rowField, gc);
		 //////////////////////////////////// SET COLUMNLABEL /////////////////////////////
		gc.gridx = 0;
		gc.gridy = 2;
		gc.anchor = GridBagConstraints.LINE_END;
		this.add(columnLabel, gc);
		/////////////////////////////////// SET COLUMNFIELD ///////////////////////////////////
		gc.gridx=1;
		gc.gridy=2;
		gc.anchor = GridBagConstraints.LINE_START;
		add(columnField, gc);
		/////////////////////////////////// SET FIREBUTTON ///////////////////////////////////
		gc.gridx=0;
		gc.gridy=3;
		gc.anchor=GridBagConstraints.LINE_END;
		add(fireBtn, gc);
		
		
				
		
	}
	
}
