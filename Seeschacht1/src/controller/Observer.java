package controller;

import seeschlacht.CellState;

public interface Observer {
	public void setSubject(Subject subj);
	public void update(int x, int y, CellState state);
	
}
