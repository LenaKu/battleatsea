package controller;

import seeschlacht.CellState;

public interface Subject {
	public void registerObserver(Observer o);
	public void unregisterObserver(Observer o);
	public void notifyObservers(int x, int y, CellState state);
	public boolean gameIsOver();
	
}
