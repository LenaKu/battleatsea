package controller;

import seeschlacht.CellState;
import seeschlacht.Ship;

public interface Shootable {
	
	void getShot(int y, int x);
	boolean gameIsOver();
	
}
