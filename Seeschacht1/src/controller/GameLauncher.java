package controller;
import javax.swing.SwingUtilities;

import seeschlacht.Seabattle;
import visual.Battlefield;
import visual.FileOutput;
import visual.GameFrame;

public class GameLauncher {

	public static void main(String[] args) {
		
		
			Seabattle game=new Seabattle();
			
			/////////////////////// File output initiiren ///////////////////
		
			FileOutput fo= new FileOutput(game);
			fo.prepareLog();
			game.registerObserver(fo);
			
			////////////////////////////// GUI Thread ///////////////////////////
			
			SwingUtilities.invokeLater(() -> {
				
				GameFrame gf= new GameFrame(game);
				gf.setVisible(true);
				
			});
		
		

	}
	
	

}
