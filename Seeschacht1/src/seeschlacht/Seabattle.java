package seeschlacht;

import java.util.ArrayList;
import java.util.Random;

import controller.Observer;
import controller.Shootable;
import controller.Subject;

public class Seabattle implements Shootable, Subject {
	private int diameter = Setup.getGridDimensions();
	private ArrayList<Deck> decksList = new ArrayList<Deck>();
	private ArrayList<Ship> fleet = new ArrayList<Ship>();
	private ArrayList<Observer> observers = new ArrayList<Observer>();
	
	

	public Seabattle() {
		startSeabattle();

	}

	
	
	private void populateDecksList() {
		for (int i = 0; i < diameter; i++) {
			for (int j = 0; j < diameter; j++) {
				decksList.add(new Deck(i, j));
			}
		}

	}

	// new method
	private void placeShipTest(Ship ship) {
		Random click = new Random();
		while (!ship.done) {
			int start = click.nextInt(diameter * diameter);
			int y = start / diameter;
			int x = start % diameter;
			int orientation = click.nextInt(2);
			switch (orientation) {
			case 0:
				ship.buildHorizontally(x, y);
				break;
			case 1:
				ship.buildVertically(x, y);
			}

			if (checkWithDecksList(ship)) {
				ship.done = true;
				secureShipsPerimeter(ship);

			}

		}
	}

	private void secureShipsPerimeter(Ship ship) {
		for (Deck deck : ship.decks) {
			for (int i = deck.x - 1; i <= deck.x + 1; i++) {
				for (int j = deck.y - 1; j <= deck.y + 1; j++) {
					Deck test = new Deck(i, j);
					
					if (decksList.contains(test)) {
						decksList.remove(test);
					}
				}
			}
		}
	}

	private boolean checkWithDecksList(Ship ship) {
		for (Deck deck : ship.decks) {
			if (decksList.contains(deck) == false)
				return false;
		}
		for (Deck deck : ship.decks) {
			decksList.remove(deck);
		}
		return true;

	}


	


	private void assembleFleet() {
		int[] modus = Setup.getNumberOfShips();
		for (int numberOfDecks : modus) {
			fleet.add(new Ship(numberOfDecks));
		}

	}

	public void startSeabattle() {
		decksList.clear();
		fleet.clear();
		populateDecksList();
		assembleFleet();
		for (Ship ship : fleet) {
			if (!ship.done) {
				placeShipTest(ship);
			//	System.out.println("Ship # " + (fleet.indexOf(ship) + 1)); //-- Zur Kontrolle
			//	ship.showShip();
			}
		}
		

	}

	
	
	
	
	public void getShot(int x, int y) {

		Deck shotAt = new Deck(x,y);
		boolean empty=true;
		for (Ship ship : fleet) {
			if (ship.shipHit2(shotAt)) {
					empty =false;
					if(ship.shipSunk()){
						for(Deck deck : ship.decks){
							notifyObservers(deck.x, deck.y, CellState.SHIP_SUNK);
						}
					//	
					}
					else 
						notifyObservers(x,y, CellState.SHIP_HIT);
				}
			
			
		}
		if(empty)notifyObservers(x,y, CellState.EMPTY);
		if (gameIsOver()) notifyObservers(x, y, CellState.GAME_OVER);
	}

	

	

	
	@Override
	public void registerObserver(Observer o) {
		observers.add(o);

	}

	@Override
	public void unregisterObserver(Observer o) {
		observers.remove(o);

	}

	@Override
	public void notifyObservers(int x, int y, CellState state) {
		for (Observer o : observers) {
			o.update(x,y,state);
			
		}

	}



	@Override
	public boolean gameIsOver() {
		for(Ship ship : fleet){
			if(!ship.dead)
				return false;
		}
		return true;
	}

	

	
	
 

}
