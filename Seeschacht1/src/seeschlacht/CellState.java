package seeschlacht;

public enum CellState {
	EMPTY,  SHIP_HIT, SHIP_SUNK, GAME_OVER;
}
