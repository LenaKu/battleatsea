package seeschlacht;

public class Setup {
	private static boolean modus=true;
	private static boolean test=false;
	private static int [] fullFleet= {4,3,3,2,2,2,1,1,1,1};
	private static int [] smallFleet= {3,3,3};
	private static int [] testFleet={2,1};
	
	public static int getGridDimensions(){
		if (test) return 3;
		if (modus) return 10;
		else return 7;
	}
	
	public static int[] getNumberOfShips(){
		if (test) return testFleet;
		if (modus) return fullFleet;
		else return smallFleet;
		
		
	}
}
