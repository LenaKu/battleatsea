package seeschlacht;

public class Ship {
	public Deck[] decks;
	boolean done=false;
	boolean dead=false;
	int numberOfDecks=3;
	
	
	public Ship(){
		this(3);
		
		
	}
	public Ship(int numberOfDecks){
		this.numberOfDecks=numberOfDecks;
		decks= new Deck [numberOfDecks];
		for (int i = 0; i < decks.length; i++) {
			decks[i]= new Deck();
		}	
	}
	
	public void showShip(){
		for (Deck deck : decks) {
			System.out.print(deck.x+" "+ deck.y+"   ");
		}
		System.out.println();
		
	}
	
	public boolean shipHit(int y,int x){
		for (Deck deck : decks) {
			if(deck.y==y &&deck.x==x){
				deck.hit=true;
				return true;
			}
			
		}
		return false;
	}
	
	public boolean shipHit(Deck cell){
		for (Deck deck : decks) {
			if(deck.y==cell.y &&deck.x==cell.x){
				deck.hit=true;
				return true;
			}
			
		}
		return false;
	}
	public boolean shipHit2(Deck cell){
		for (Deck deck : decks) {
			if(cell.equals(deck)){
				deck.hit=true;
				return true;
			}
			
		}
		return false;
	}
	
	public boolean shipSunk(){
		for (Deck deck : decks) {
			if(!deck.hit){
				
				return false;
			}
			
		}
		dead=true;
		return dead;
	}
	public void buildHorizontally(int x, int y){
		for (int i = 0; i <decks.length; i++) {
				decks[i].x=x+i;
				decks[i].y=y;
		}
	}
	public void buildVertically(int x, int y){
		for (int i = 0; i <decks.length; i++) {
				decks[i].x=x;
				decks[i].y=y+i;
		}
	}
	
}
