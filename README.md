Das Spiel Battle at Sea ist ein Lernpojekt zur Implementierung von MVC Model sowie Subject/Observer Pattern. 
Es hat zwei verschiedene Input Fenster, die unabhängig von einander funktionieren. Zum Spielspaß tragen sie zugegebenerweise nicht bei. 

Zum Spielstart werden 10 Schiffe im Feld positioniert, ein 4-Decker, 2 x 3-Decker, 3 x 2-Decker und 4 x 1-Decker. 
Die Decks sind immer in einer Linie angeordnet und Schiffe können nicht aneinander anliegen. Das Spiel endet, wenn alle Schiffe versenkt sind. 